'use strict';
const Generator = require('yeoman-generator');
const chalk = require('chalk');
const yosay = require('yosay');

module.exports = class extends Generator {
  prompting() {
    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the fantabulous ' + chalk.red('generator-react-jest-code-generator') + ' generator!'
    ));

    const prompts = [{
      type: 'input',
      name: 'moduleName',
      message: 'Choose a name for your module? must be lowercase with space',
      default: 'add gateway modal view'
    },
    {
      type: 'list',
      name: 'mode',
      message: 'select mode to generate Jest code',
      choices: ['view', 'state'],
      default: 'module'
    }];

    return this.prompt(prompts).then(props => {
      // To access props later use this.props.someAnswer;
      this.props = props;
    });
  }

  writing() {
    const jsonProperties = this.fs.readJSON(this.templatePath('viewProperties.txt'));
    console.log(jsonProperties);
  }

  install() {
    this.installDependencies();
  }
};
