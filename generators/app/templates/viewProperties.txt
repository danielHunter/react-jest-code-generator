SensorsView.propTypes = {
  t: PropTypes.func.isRequired,
  getSensors: PropTypes.func.isRequired,
  initializeConfirmModalShow: PropTypes.func.isRequired,
  deleteSensor: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  closeEditSensorPanel: PropTypes.func.isRequired,
  showEditSensorPanel: PropTypes.bool.isRequired,
  openEditSensorPanel: PropTypes.func.isRequired,
  editSensorPanelStep: PropTypes.number.isRequired,
  setEditSensorPanelStep: PropTypes.func.isRequired,
  sensors: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    inputId: PropTypes.number,
    inputName: PropTypes.string,
    modelId: PropTypes.number,
    modelName: PropTypes.string,
    name: PropTypes.string,
    zones: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      name: PropTypes.string
    })).isRequired
  })).isRequired,
  zones: PropTypes.arrayOf(PropTypes.shape({

  })).isRequired,
  defaultSensor: PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    modelId: PropTypes.number,
    inputId: PropTypes.number,
    zoneIds: PropTypes.arrayOf(PropTypes.number)
  }).isRequired,
  sensorLists: PropTypes.shape({
    flowsensorModelsGrouped: PropTypes.arrayOf(PropTypes.shape({
      categoryId: PropTypes.number,
      categoryName: PropTypes.string,
      models: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.number,
        modelName: PropTypes.string
      }))
    })),
    flowsensorInputs: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number,
      value: PropTypes.string
    }))
  }).isRequired,
  openEditCustomSensorTypePanel: PropTypes.func.isRequired,
  updateSensor: PropTypes.func.isRequired,
  updateLoading: PropTypes.bool.isRequired
};
