# generator-react-jest-code-generator [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> Generate React Jest test code, for Hydrawise, a Hunter Industry product.

## Installation

First, install [Yeoman](http://yeoman.io) and generator-react-jest-code-generator using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
```
Go to the git repository and
```bash
npm link
```
Then navigate to your target project repo and generate code with:
```bash
yo react-jest-code-generator
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [Daniel Sun]()


[npm-image]: https://badge.fury.io/js/generator-react-jest-code-generator.svg
[npm-url]: https://npmjs.org/package/generator-react-jest-code-generator
[travis-image]: https://travis-ci.org//generator-react-jest-code-generator.svg?branch=master
[travis-url]: https://travis-ci.org//generator-react-jest-code-generator
[daviddm-image]: https://david-dm.org//generator-react-jest-code-generator.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//generator-react-jest-code-generator
